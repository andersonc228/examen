/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

import java.io.*;

/**
 *
 * @author ander
 */
public class Adn {

    /**
     * Funcion que me busca una secuencia si la encuntra la borra y la imprime
     * sin la parte encontrada
     *
     * @param cadena cadena de texto que viene del fichero
     * @param secuencia secuencia a buscar dentro de mi fichero
     */
    public static void borrar(String cadena, String secuencia) {

        String array[];
        //en esta parte me busca si contiene esa secuencia
        if (cadena.contains(secuencia)) {
            //como ha econtrado la secuencia la parte en ese lugar
            array = cadena.split(secuencia);
            String temp2 = array[0];
            String temp1 = array[1];
            //aca me concatena las 2 partes para asi obtener una sola
            System.out.println("Secuencia " + temp1.concat(temp2));
        } else {
            //mensaje de error cuando no ha encontrado la secuencia
            System.out.println("No se encuentra la secuencia");
        }
    }

    /**
     * Funcion que por parametro le entra el texto del fichero en el cual vamos
     * a buscar una parte ingresada por el usuario si es true me imprime y si es
     * false da un mensaje de error
     *
     * @param cadena cadena de texto en el fichero
     * @param busqueda parte a buscar en el fichero
     */
    public static void buscar(String cadena, String busqueda) {

        String res;
        //me busca si contiene cadena la secuencia ingresada y me la imprime
        if (cadena.contains(busqueda)) {
            System.out.println(cadena);
        } else {
            //si no encuentra la secuencia dentro de la cadena me imprime un mensaje de error
            System.out.println("No hay ningun trozo con esos caracteres");
        }

    }

}
