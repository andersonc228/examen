package examen;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author anderson Coronado
 */
public class MainFichero {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);

        //apertura del fichero
        String file = "F:\\Anderson\\PROVENÇANA\\DAWBIO2\\M15_JAVA\\examen\\examen\\secuenciaADN.txt";
        String cadena;
        //me lee el fichero 
        FileReader f = new FileReader(file);
        BufferedReader b = new BufferedReader(f);
        //me guarda el texto en la variabele cadena
        cadena = b.readLine();

        int opcion = 0;
        while (opcion != 3) {
            System.out.println("1. Borrar\n"
                    + "2. Buscar\n"
                    + "3. Salir");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    System.out.println("Borrar");
                    Adn.borrar(cadena, "gat");
                    break;
                case 2:
                    System.out.println("Buscar");
                    Adn.buscar(cadena, "gat");
                    break;
                case 3:
                    System.out.println("Adios");
                    break;

            }
        }
    }
}
